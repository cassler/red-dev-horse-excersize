<?php 

// Register Custom Post Type
function redDev_horse_cpt() {

  $labels = array(
    'name'                  => _x( 'Horses', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'Horse', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'Horses', 'text_domain' ),
    'name_admin_bar'        => __( 'Horses', 'text_domain' ),
    'archives'              => __( 'Horse Archive', 'text_domain' ),
    'attributes'            => __( 'Horse Attributes', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Horse', 'text_domain' ),
    'all_items'             => __( 'All Horses', 'text_domain' ),
    'add_new_item'          => __( 'Add New Horse', 'text_domain' ),
    'add_new'               => __( 'Add Horse', 'text_domain' ),
    'new_item'              => __( 'New Horse', 'text_domain' ),
    'edit_item'             => __( 'Edit Horse', 'text_domain' ),
    'update_item'           => __( 'Update Horse', 'text_domain' ),
    'view_item'             => __( 'View Horse', 'text_domain' ),
    'view_items'            => __( 'View Horses', 'text_domain' ),
    'search_items'          => __( 'Search Horses', 'text_domain' ),
    'not_found'             => __( 'Horse Not Found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Horse Not found in Trash', 'text_domain' ),
    'featured_image'        => __( 'Horse Portrait Image', 'text_domain' ),
    'set_featured_image'    => __( 'Set horse portrait', 'text_domain' ),
    'remove_featured_image' => __( 'Remove horse portrait', 'text_domain' ),
    'use_featured_image'    => __( 'Use as horse portrait', 'text_domain' ),
    'insert_into_item'      => __( 'Insert into horse profile', 'text_domain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this horse profile', 'text_domain' ),
    'items_list'            => __( 'Horse list', 'text_domain' ),
    'items_list_navigation' => __( 'Horse list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter horse list', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'Horse', 'text_domain' ),
    'description'           => __( 'A collection of horses', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor' ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'horse', $args );

}
add_action( 'init', 'redDev_horse_cpt', 0 );

?>