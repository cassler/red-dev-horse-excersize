## Environment

- MAMP 
- PHP/7.2.1 
- Apache
- WordPress 4.9.6 (as provided)
- Plugins (all versions as provided)

## Setup & Debug

- Create empty DB to prepare for import.
- Import provided database backup to local SQL server
- Modify `wp_config.php` with database connection details
- Set `WP_DEBUG', true` <-- super important

### Debug, Routing & Compatability

**First notice:** `wp_options > siteurl` and `wp_options > home` set as `http://reddevexercise`. 
Soltions: add values to /etc/hosts or modify to `localhost'

**Fatal error:** 
`Uncaught Error: Class 'Twig_SimpleFunction.` Traces to 'responsive-menu' plugin. It seems that there is an issue with the plugin files themeselves. Deleting and re-installing the plugin will remedy the problem.

The following is not entirely necessary because the WordPress dashboard continues to work despite the fatal error. However, since plugins often cause 'white screen of death' across the application, the above steps can be used. Since this has caused a 500 on the front-end, plugin can be disabled from a WP-CLI (or database changes. `wp_options > active_plugins`). 

`
a:8:{i:0;s:30:"advanced-custom-fields/acf.php";i:1;s:53:"automatic-plugin-updates/automatic-plugin-updates.php";i:2;s:21:"backwpup/backwpup.php";i:3;s:23:"edit-flow/edit_flow.php";i:5;s:33:"w3-total-cache/w3-total-cache.php";i:6;s:24:"wordpress-seo/wp-seo.php";i:7;s:24:"wpforms-lite/wpforms.php";}
`

change to -> 

`
a:8:{i:0;s:30:"advanced-custom-fields/acf.php";i:1;s:53:"automatic-plugin-updates/automatic-plugin-updates.php";i:2;s:21:"backwpup/backwpup.php";i:3;s:23:"edit-flow/edit_flow.php";i:5;s:33:"w3-total-cache/w3-total-cache.php";i:6;s:24:"wordpress-seo/wp-seo.php";i:7;s:24:"wpforms-lite/wpforms.php";}
`

## The Horses Custom Post Type

- register new custom post function on `init`
- require custom post function file into `functions.php`
- duplicate and modify `archive.php` from parent theme.
- use to create `archive-horse.php`
- `wp_enqueue_style` for `horses.css`
- remove the sidebar
- new template part for `horse-index`
- use CSS Grids to accomplish layout goals in `horse.css`

